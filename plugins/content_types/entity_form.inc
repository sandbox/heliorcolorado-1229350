<?php

$plugin = array(
  'title' => t('Entity Form'),
  'description' => t('Provide form builders as content.'),
  'content types' => 'entity_form_content_types',
  'render callback' => 'entity_form_content_type_render',
  'edit form' => 'entity_form_content_type_edit_form',
  'admin title' => 'entity_form_content_type_admin_title',
);


/**
 * 'Content types' callback.
 */
function entity_form_content_types() {
  $types = array();
  
  foreach (entity_get_info() as $name => $info) {
    // We're only interested in entities that use the Entity API's UI controller.
    if (!isset($info['admin ui'])) {
      continue;
    }
    
    // Create a sub-type for each operation (add, edit, clone, etc.)
    foreach (entity_form_ops() as $op => $op_info) {
      $types[$name . ':' . $op] = array(
        'name' => $name,
        'title' => t('!operation !entity_label Form', array('!entity_label' => $info['label'], '!operation' => $op_info['label'])),
        'icon' => $op . '.png',
        'category' => t('Entity Forms'),
      );
      
      // Some operation require a fully loaded entity context.
      if (isset($op_info['requires context']) && $op_info['requires context']) {
        $types[$name . ':' . $op]['required context'] = new ctools_context_required($info['label'], 'entity:' . $name);
      }
    }
  }
  
  return $types;  
}

/**
 * 'Render callback' callback.
 */
function entity_form_content_type_render($subtype, $conf, $args, $context) {
  $info = entity_get_info();
  list($entity_type, $op) = explode(':', $subtype);
  
  // Load admin ui include file
  entity_form_include_ui($entity_type);

  if ($op != 'add' && $context->data) {
    $entity = $context->data;
  }
  else {
    $entity = NULL;
  }
  
  // Build content object
  $content = new stdClass();
  $content->content = entity_ui_get_form($entity_type, $entity, $op);

  return $content;
}

/**
 * 'Edit form' callback.
 */
function entity_form_content_type_edit_form($form, &$form_state) {
  // Just here to override the title.
  return $form;
}

/**
 * 'Admin title' callback.
 */
function entity_form_content_type_admin_title($subtype, $conf, $pane_context) {
  $info = entity_form_content_types();
  $prefix = t('Entity Form') . ': ';

  // Return human-readable name...
  if (isset($info[$subtype])) {
    return $prefix . $info[$subtype]['title'];
  }
  
  // ... otherwise just return the machine-name
  return $prefix . $subtype;
}

/**
 * Stores form operation meta information.
 */
function entity_form_ops() {
  return array(
    'add' => array(
      'label' => t('Add'),
    ),
    'edit' => array(
      'label' => t('Edit'),
      'requires context' => TRUE,
    ),
    'clone' => array(
      'label' => t('Clone'),
      'requires context' => TRUE,
    ),
  );
}
